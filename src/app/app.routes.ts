import { provideRouter, RouterConfig } from '@angular/router';

import { TestComponent } from './test/test.component';

const routes: RouterConfig = [
  { path: '', redirectTo: '/test', terminal: true },
  {path: 'test', component: TestComponent}
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
