import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'test',
    templateUrl: 'test.component.html'
})
export class TestComponent implements OnInit {

    poruka: string = 'Pošte Srpske 2016';
    
    constructor() { }

    ngOnInit() { }
}